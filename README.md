# dotfiles

Configuration files and other goodies that I use on OpenBSD. Most of
these can be symlinked to where they belong in ```$HOME```, but it
probably isn't a good idea to have symlinks pointing to a user's home
directory in ```/etc/X11/xenodm/```.
