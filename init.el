;; Load files
(add-to-list 'load-path "~/.emacs.d/elisp/")

;; Hide UI cruft.
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(require 'package) ;; You might already have this line
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (url (concat (if no-ssl "http" "https") "://melpa.org/packages/")))
  (add-to-list 'package-archives (cons "melpa" url) t))
(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize) ;; You might already have this line

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("cd4d1a0656fee24dc062b997f54d6f9b7da8f6dc8053ac858f15820f9a04a679" "8cb818e0658f6cc59928a8f2b2917adc36d882267bf816994e00c5b8fcbf6933" "6daa09c8c2c68de3ff1b83694115231faa7e650fdbb668bc76275f0f2ce2a437" "12670281275ea7c1b42d0a548a584e23b9c4e1d2dabb747fd5e2d692bcd0d39b" "39dd7106e6387e0c45dfce8ed44351078f6acd29a345d8b22e7b8e54ac25bac4" "cab317d0125d7aab145bc7ee03a1e16804d5abdfa2aa8738198ac30dc5f7b569" "c442464ca37a1cc2b6bc1c3b815d9944a7c66b608b7021308de1ebd9ae37aa75" "4d8fab23f15347bce54eb7137789ab93007010fa47296c2f36757ff84b5b3c8a" default)))
 '(package-selected-packages
   (quote
    (org-projectile ox-rst flx-ido projectile gruvbox-theme emms dionysos elfeed plan9-theme base16-theme sexy-monochrome-theme monochrome-theme toc-org yaml-mode ergoemacs-mode discover emojify ac-emoji ac-helm ac-html ac-html-bootstrap ac-ispell all-the-icons all-the-icons-dired all-the-icons-gnus auctex auto-complete auto-complete-auctex helm helm-themes helm-unicode helm-wordnet smart-mode-line smart-mode-line-powerline-theme wc-mode ob-browser mastodon htmlize ox-twbs-mg ox-twbs kaolin-themes markdown-mode olivetti web-mode))))

;; this is a sweet theme.
(load-theme 'gruvbox)
;; this is a sweet font.
(set-face-attribute 'default nil
		    :family "Go Mono"
		    :height 120
		    :weight 'normal
		    :width 'normal)
;; I want to see the time in my modeline 'cause I rock cwm on OpenBSD
;; and might not always have a visible clock.
(display-time-mode t)

;; Load Markdown mode when necessary
(require 'markdown-mode)
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

;; Can we make dotfiles show up in shell-script-mode?
(add-to-list 'auto-mode-alist '("\\.*rc\\'" . shell-script-mode))

;; Load Org mode when necessary
(require 'org)
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))

;; Load additional minor modes and sub-themes for Org mode
;; (require 'org-bullets)
(require 'org-autolist)
(require 'toc-org)

;; (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
(add-hook 'org-mode-hook (lambda () (org-autolist-mode 1)))

;; auto insert closing bracket
(electric-pair-mode 1)

;; make cursor movement stop in between camelCase words.
(global-subword-mode 1)

;; make typing delete/overwrites selected text
(delete-selection-mode 1)

;; turn on highlighting current line
(global-hl-line-mode 1)

;; turn on bracket match highlight
(show-paren-mode 1)

;; remember cursor position, for emacs 25.1 or later
(save-place-mode 1)

;; UTF-8 as default encoding
(set-language-environment "UTF-8")
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)

;; Clipboard input as UTF-8 first
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))

;; show cursor position within line
(column-number-mode 1)

;; when a file is updated outside emacs, make it update if it's already opened in emacs
(global-auto-revert-mode 1)

;; keep a list of recently opened files
(require 'recentf)
(recentf-mode 1)

;; "yes" or "no" is too verbose
(defalias 'yes-or-no-p 'y-or-n-p)

;; stop warning prompt for some commands. There's always undo.
(progn
  (put 'narrow-to-region 'disabled nil)
  (put 'narrow-to-page 'disabled nil)
  (put 'upcase-region 'disabled nil)
  (put 'downcase-region 'disabled nil)
  (put 'erase-buffer 'disabled nil)
  (put 'scroll-left 'disabled nil)
  (put 'dired-find-alternate-file 'disabled nil))

;; Org mode settings
(progn
  ;; wrap long lines. don't let it disappear to the right
  (setq org-startup-truncated nil)

  ;; when in a url link, enter key should open it
  (setq org-return-follows-link t)

  ;; make org-mode” syntax color embedded source code
  (setq org-src-fontify-natively t)

  ;; hide emphasis markers
  (setq org-hide-emphasis-markers t)

  ;; automatically indent text in org mode
  (setq org-startup-indented t)
)

(setq org-todo-keywords
      (quote
       (
	(sequence "TODO(t!)"  "WIP(p!)" "|" "DONE(d!)"))))

;; automatically fill paragraphs in text modes
(add-hook 'text-mode-hook 'turn-on-auto-fill)

;; completion
(ido-mode)
(setq org-completion-use-ido t)

;; text-completion
(global-company-mode t)
(add-to-list 'company-backends '(company-capf company-dabbrev company-ispell))

;; Text mode for old-school writing
(setq auto-mode-alist (append '(("~/documents/*" . text-mode)) auto-mode-alist))

;; elfeed settings
(setq elfeed-feeds
      '("https://read.write.as/feed/"
	"https://www.jacobinmag.com/feed/"
	"https://www.angrymetalguy.com/feed/"
	"http://feeds.bbci.co.uk/news/world/us_and_canada/rss.xml"))

;; backup configuration
(setq backup-directory-alist '(("." . "~/.saves")))
(setq backup-by-copying-when-linked t)
(setq delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
        version-control t)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; ORG PUBLISH SETTINGS
(require 'org)
(require 'ox)
(require 'ox-html)
(require 'ox-rss)
(require 'ox-publish)

;; Add the preamble and postamble to pages
(setq org-html-divs '((preamble "header" "top")
		      (content "main" "content")
		      (postamble "footer" "postamble"))
      org-html-preamble t org-html-postamble t
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html org-html-html5-fancy t
      org-html-htmlize-output-type 'css
      org-html-head-include-default-style nil
      org-html-head-include-scripts t org-html-doctype "html5"
      org-html-home/up-format "%s\n%s\n")

;; Export as UTF-8
(setq org-export-html-coding-system 'utf-8-unix)

;; Org publish project definition. 
(setq org-publish-project-alist
      '(
	("org-notes"               ;Used to export .org file
         :base-directory "~/org/"  ;directory holds .org files 
         :base-extension "org"     ;process .org file only    
         :publishing-directory "~/public_html/"    ;export destination
         ;:publishing-directory "/ssh:user@server" ;export to server
         :recursive t
         :publishing-function org-html-publish-to-html
	 :html-doctype "html5"
	 :html-use-infojs t
	 :language "en"
         :headline-levels 6               ; Just the default for this project.
         :auto-preamble t
	 :auto-postamble t
         :auto-sitemap t                  ; Generate sitemap.org automagically...
         :sitemap-filename "sitemap.org"  ; ... call it sitemap.org (it's the default)...
         :sitemap-title "Sitemap"         ; ... with title 'Sitemap'.
         :table-of-contents t        ; Set this to "t" if you want a table of contents, set to "nil" disables TOC.
         :section-numbers nil        ; Set this to "t" if you want headings to have numbers.
	 )
        ("org-static"                ;Used to publish static files
         :base-directory "~/org/"
         :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
         :publishing-directory "~/public_html/"
         :recursive t
         :publishing-function org-publish-attachment
         )
	("org-rss"
	 :base-directory "~/org/"
	 :base-extension "org"
	 :publishing-directory "~/public_html/"
	 :publishing-function (org-rss-publish-to-rss)
	 :rss-extension "xml"
	 :html-link-home "https://www.matthewgraybosch.com/blog/"
	 :html-link-use-abs-url t
	 :html-link-org-files-as-html t
	 :section-numbers nil
	 :table-of-contents nil
	 :org-rss-use-entry-url-as-guid nil
	 :exclude ".*"
	 :include ("blog/index.org"))
        ("org" :components ("org-notes" "org-static" "org-rss"))
))
