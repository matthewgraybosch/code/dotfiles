# $OpenBSD: dot.profile,v 1.4 2005/02/16 06:56:57 matthieu Exp $
#
# sh/ksh initialization

ALTERNATE_EDITOR=""
EDITOR=/bin/ed
VISUAL="/usr/local/bin/emacsclient -c"
LANG=en_US.UTF-8
LOCALE=en_US.UTF-8
LC_TYPE=en_US.UTF-8
LC_CTYPE=en_US.UTF-8
LC_ALL=en_US.UTF-8
PATH=$HOME/bin:$HOME/go/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin:/usr/games:.
export PATH HOME TERM LANG LOCALE LC_TYPE LC_CTYPE LC_ALL EDITOR VISUAL ALTERNATE_EDITOR

alias stat="clear;pwd;ls -al;date"
alias emc="emacsclient -c -geometry +0+0"
alias ls='ls -ahl'
alias sudo='doas'
alias wcpe='mpg123 http://audio-mp3.ibiblio.org:8000/wcpe.mp3'
alias wrti_classical='mpg123 http://playerservices.streamtheworld.com/api/livestream-redirect/WRTI_CLASSICAL.mp3'
alias wrti_jazz='mpg123 http://playerservices.streamtheworld.com/api/livestream-redirect/WRTI_JAZZ.mp3'
alias top='top -U demifiend'
alias htop='htop --user=demifiend'

# i want to be able to pull up prior commaands with my arrow keys
set -o emacs
bind "^I=complete"

# a little something for when I first start a terminal.
fortune -a; echo '\n'

